﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace kzApps
{
    [CreateAssetMenu(menuName = "Actions/Player Actions/Load On Active Holder")]
    public class LoadOnActiveHolder : PlayerAction
    {
        public override void Execute(PlayerHolder player)
        {
            GameManager.singleton.LoadPlayerOnActive(player);
        }
    }
}
