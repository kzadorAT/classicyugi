﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace kzApps
{
    [CreateAssetMenu(menuName = "Actions/Reset Change Position")]
    public class ResetChangePosition : PlayerAction
    {
        public override void Execute(PlayerHolder player)
        {
            foreach (CardInstance c in player.MonsterCards)
            {
                if (!c.canChangePosition)
                {
                    c.canChangePosition = true;
                }
            }
        }
    }
}
