﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace kzApps
{
    [CreateAssetMenu(menuName = "Actions/Player Actions/Draw Card")]
    public class DrawCard : PlayerAction
    {
        public override void Execute(PlayerHolder player)
        {
            GameManager.singleton.DrawCard(player);
        }
    }
}