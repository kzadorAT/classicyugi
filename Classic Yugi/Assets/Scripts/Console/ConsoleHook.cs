﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace kzApps
{
    [CreateAssetMenu(menuName ="Console/Hook")]
    public class ConsoleHook : ScriptableObject
    {
        [System.NonSerialized]
        public ConsoleManager consoleManager;

        public void RegisterEvent(string s, Color color)
        {
            consoleManager.RegisterEvent(s, color);
        }
    }
}
