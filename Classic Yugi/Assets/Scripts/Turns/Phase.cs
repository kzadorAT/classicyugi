﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace kzApps
{
    public abstract class Phase : ScriptableObject
    {
        public string phaseName;
        public bool forceExit;
        public abstract bool IsComplete();

        [System.NonSerialized]
        protected bool isInit;

        public abstract void OnStartPhase();

        public abstract void OnEndPhase();
    }
}
