﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kzApps
{
    [CreateAssetMenu(menuName = "Turns/Battle Resolve")]
    public class BattleResolvePhase : Phase
    {
        public Element attackElement;
        public Element defenseElement;

        public override bool IsComplete()
        {
            if (forceExit)
            {
                forceExit = false;
                return true;
            }

            return false;
        }

        public override void OnEndPhase()
        {
        }

        public override void OnStartPhase()
        {
            PlayerHolder p = Settings.gameManager.currentPlayer;
            PlayerHolder e = Settings.gameManager.GetEnemyOf(p);

            if(p.currentAttackingCard == null)
            {
                forceExit = true;
                return;
            }

            CardInstance inst = p.currentAttackingCard;
            Card c = inst.viz.card;
            CardProperties attack = c.GetProperty(attackElement);
            if (attack == null)
            {
                Debug.LogError("You are attacking with a card that can't attack!");
            }
            e.DoDamage(attack.intValue);


            p.currentAttackingCard = null;
            forceExit = true;
        }
    }
}
