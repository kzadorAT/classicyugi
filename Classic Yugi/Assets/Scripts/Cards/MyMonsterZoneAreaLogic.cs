﻿using UnityEngine;
using System.Collections;
using kzApps.GameElements;

namespace kzApps
{
    [CreateAssetMenu(menuName = "Areas/MyMonsterZoneWhenHoldingCard")]
    public class MyMonsterZoneAreaLogic : AreaLogic
    {
        public CardVariable card;
        public CardType monsterCard;
        public SO.TransformVariable areaGrid;
        public GE_Logic MonsterCardLogic;
        public Position attackPosition;
        public Position defensePosition;
        public override void Execute(Position position)
        {
            if (card.value == null)
                return;

            if (card.value.viz.card.cardType == monsterCard && areaGrid.value.childCount < 5)
            {
                if (attackPosition == position)
                {
                    card.value.IsInDefensePosition = false;
                }
                if (defensePosition == position)
                {
                    card.value.IsInDefensePosition = true;
                }
                Settings.DropMonsterCard(card.value.transform, areaGrid.value.transform, card.value);
                card.value.gameObject.SetActive(true);
                card.value.currentLogic = MonsterCardLogic;
                // Place card down
            }
        }
    }
} 
