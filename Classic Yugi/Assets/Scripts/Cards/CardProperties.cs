﻿using UnityEngine;
using System.Collections;

namespace kzApps
{
    [System.Serializable]
    public class CardProperties
    {
        public string stringValue;
        public int intValue;
        public Sprite sprite;
        public Element element;
    } 
}
