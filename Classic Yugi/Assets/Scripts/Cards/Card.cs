﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kzApps
{
    [CreateAssetMenu(menuName = "Card")]
    public class Card : ScriptableObject
    {
        public CardType cardType;
        public CardProperties[] properties;

        public CardProperties GetProperty(Element e)
        {
            for (int i = 0; i < properties.Length; i++)
            {
                if(properties[i].element == e)
                {
                    return properties[i];
                }
            }

            return null;
        }
    } 
}
