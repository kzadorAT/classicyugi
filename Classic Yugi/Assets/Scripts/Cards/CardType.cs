﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kzApps
{
    public abstract class CardType : ScriptableObject
    {
        public string typeName;
        public bool canAttack;
        // public typeLogic logic;

        public virtual void OnSetType(CardViz viz)
        {

            Element t = Settings.GetResourcesManager().typeElement;
            CardVizProperties type = viz.GetProperty(t);
            type.text.text = typeName;

        }

        public bool EffectAllowsForAttack(CardInstance inst)
        {
            // e.g. Super heavy Samurais monsters can attack even if in defense position
            // bool r = logic.Execute(inst) -> if(inst.isInDefensePosition)/inst.isInDefensePosition = false; return true;

            if (canAttack)
                return true;
            else
                return false;
        }
    } 
}
