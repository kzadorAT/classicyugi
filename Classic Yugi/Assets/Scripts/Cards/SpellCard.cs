﻿using UnityEngine;
using System.Collections;

namespace kzApps
{
    [CreateAssetMenu(menuName = "Cards/Spell")]
    public class SpellCard : CardType
    {
        public override void OnSetType(CardViz viz)
        {
            base.OnSetType(viz);
            viz.typeHolder.SetActive(false);
            viz.starsHolder.SetActive(false);
            viz.atkDefHolder.SetActive(false);
        }
    } 
}
