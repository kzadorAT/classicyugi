﻿using UnityEngine;
using System.Collections;

namespace kzApps
{
    [CreateAssetMenu(menuName = "Cards/Trap")]
    public class TrapCard : CardType
    {
        public override void OnSetType(CardViz viz)
        {
            base.OnSetType(viz);
            viz.typeHolder.SetActive(false);
            viz.starsHolder.SetActive(false);
            viz.atkDefHolder.SetActive(false);
        }
    }

}