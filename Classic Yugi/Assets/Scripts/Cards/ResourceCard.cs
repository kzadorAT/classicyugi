﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace kzApps
{
    [CreateAssetMenu(menuName = "Cards/Monster")]
    public class ResourceCard : CardType
    {
        public override void OnSetType(CardViz viz)
        {
            base.OnSetType(viz);
            viz.typeHolder.SetActive(false);
            viz.starsHolder.SetActive(false);
        }
    }
}
