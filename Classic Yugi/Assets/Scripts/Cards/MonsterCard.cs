﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace kzApps
{
    [CreateAssetMenu(menuName = "Cards/Monster")]
    public class MonsterCard : CardType
    {
        public Element level;
        public GameObject star;

        public override void OnSetType(CardViz viz)
        {
            base.OnSetType(viz);
            viz.typeHolder.SetActive(true);
            viz.starsHolder.SetActive(true);
            viz.atkDefHolder.SetActive(true);

            foreach (Transform child in viz.starsHolder.transform)
            {
                Destroy(child.gameObject);
            }

            for (int i = 0; i < viz.card.GetProperty(level).intValue; i++)
            {
                Instantiate(star, viz.starsHolder.transform);
            }
        }
    }

}