﻿using UnityEngine;
using System.Collections;
namespace kzApps.GameElements
{

    [CreateAssetMenu(menuName = "Game Elements/My SpellTrap Card")]
    public class MySTCard : GE_Logic
    {
        public override void OnClick(CardInstance inst)
        {
            Debug.Log("This card is mine but is on the spell trap card zones.");
        }

        public override void OnHighlight(CardInstance inst)
        {

        }
    }

}