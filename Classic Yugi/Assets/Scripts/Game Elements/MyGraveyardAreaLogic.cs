﻿using UnityEngine;
using System.Collections;
using kzApps.GameElements;

namespace kzApps
{
    [CreateAssetMenu(menuName = "Areas/MyGraveyard")]
    public class MyGraveyardAreaLogic : AreaLogic
    {
        public CardVariable card;
        public CardType trapCard;
        public CardType spellCard;
        public CardType monsterCard;
        public SO.TransformVariable areaGrid;
        public GE_Logic MyGraveyardLogic;
        public override void Execute(Position position)
        {
            if (card.value == null)
                return;

            if(card.value.viz.card.cardType == trapCard || card.value.viz.card.cardType == spellCard || card.value.viz.card.cardType == monsterCard)
            {
                Settings.SetParentForCard(card.value.transform, areaGrid.value.transform);
                card.value.gameObject.SetActive(true);
                card.value.currentLogic = MyGraveyardLogic;
                Settings.gameManager.currentPlayer.AddGraveyardCard(card.value.gameObject);
                // Place card down
            }
        }
    }
}