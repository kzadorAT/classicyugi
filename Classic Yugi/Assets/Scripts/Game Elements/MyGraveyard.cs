﻿using UnityEngine;
using System.Collections;
namespace kzApps.GameElements
{

    [CreateAssetMenu(menuName = "Game Elements/My Graveyard Card")]
    public class MyGraveyard : GE_Logic
    {
        public override void OnClick(CardInstance inst)
        {
            Debug.Log("This card is mine but is on the graveyard");
        }

        public override void OnHighlight(CardInstance inst)
        {

        }
    }

}