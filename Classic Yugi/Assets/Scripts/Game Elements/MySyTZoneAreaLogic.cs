﻿using UnityEngine;
using System.Collections;
using kzApps.GameElements;

namespace kzApps
{
    [CreateAssetMenu(menuName = "Areas/MySyTZoneWhenHoldingCard")]
    public class MySyTZoneAreaLogic : AreaLogic
    {
        public CardVariable card;
        public CardType trapCard;
        public CardType spellCard;
        public SO.TransformVariable areaGrid;
        public GE_Logic SyTCardLogic;
        public override void Execute(Position position)
        {
            if (card.value == null)
                return;

            if((card.value.viz.card.cardType == trapCard || card.value.viz.card.cardType == spellCard) && areaGrid.value.childCount < 5)
            {
                Settings.DropSyTCard(card.value.transform, areaGrid.value.transform, card.value);
                card.value.gameObject.SetActive(true);
                card.value.currentLogic = SyTCardLogic;
                // Place card down
            }
        }
    }
}