﻿using UnityEngine;
using System.Collections;
namespace kzApps.GameElements
{

    [CreateAssetMenu(menuName = "Game Elements/My Monster Card")]
    public class MyMonsterCard : GE_Logic
    {
        public override void OnClick(CardInstance inst)
        {
            Debug.Log("This card is mine but is on the monster card zones on attack position.");
        }

        public override void OnHighlight(CardInstance inst)
        {

        }
    }

}