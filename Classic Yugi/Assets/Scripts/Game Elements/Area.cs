﻿using UnityEngine;
using System.Collections;

namespace kzApps.GameElements
{
    public class Area : MonoBehaviour
    {

        public AreaLogic logic;
        public Position position;
        public void OnDrop()
        {
            logic.Execute(position);
        }
    }
}