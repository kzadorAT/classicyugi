﻿using UnityEngine;
using System.Collections;

namespace kzApps.GameElements
{
    [CreateAssetMenu(menuName ="Game Elements/My Hand Card")]
    public class HandCard : GE_Logic
    {
        public SO.GameEvent OnCurrentCardSelected;
        public CardVariable currentCard;
        public kzApps.GameStates.State holdingCard;
        public override void OnClick(CardInstance inst)
        {
            currentCard.Set(inst);
            Settings.gameManager.SetState(holdingCard);
            OnCurrentCardSelected.Raise();
        }

        public override void OnHighlight(CardInstance inst)
        {
            
        }
    }

}