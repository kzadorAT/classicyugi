﻿using UnityEngine;
using System.Collections;

namespace kzApps
{
    public abstract class AreaLogic : ScriptableObject
    {
        public abstract void Execute(Position position);
    }
}