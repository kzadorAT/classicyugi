﻿using UnityEngine;

namespace kzApps.GameElements
{
    [CreateAssetMenu(menuName = "Game Elements/My Monsters on Attack Position")]
    public class MyMonstersAtkPos : GE_Logic
    {
        public override void OnClick(CardInstance inst)
        {
            Debug.Log("This card is mine but is a monster on attack position.");
        }

        public override void OnHighlight(CardInstance inst)
        {

        }
    }
}
