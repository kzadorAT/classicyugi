﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using kzApps.GameElements;
using UnityEngine;

namespace kzApps
{
    public class CardInstance : MonoBehaviour, IClickable
    {
        public CardViz viz;
        public GE_Logic currentLogic;
        public bool IsInDefensePosition = false;
        public bool canChangePosition;


        //public void SetDefensePosition(bool inDefPosition)
        //{
        //    IsInDefensePosition = inDefPosition;
        //    if (inDefPosition)
        //        transform.localEulerAngles = new Vector3(0, 0, 90);
        //    else
        //        transform.localEulerAngles = Vector3.zero;
        //}

        void Start()
        {
            viz = GetComponent<CardViz>();
        }

        public bool CanAttack()
        {
            bool result = true;

            if (IsInDefensePosition)
                result = false;

            if (viz.card.cardType.EffectAllowsForAttack(this))
                result = true;

            return result;
        }

        public void OnClick()
        {
            if (currentLogic == null)
                return;

            currentLogic.OnClick(this);
        }

        public void OnHighlight()
        {
            if (currentLogic == null)
                return;

            currentLogic.OnHighlight(this);
        }
    }
}
