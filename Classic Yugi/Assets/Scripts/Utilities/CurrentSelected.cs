﻿using kzApps.GameElements;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace kzApps
{
    public class CurrentSelected : MonoBehaviour
    {
        public CardVariable currentCard;
        public CardViz cardViz;

        Transform mTransform;

        public CardType monsterCard;
        public Position defensePosition;
        public Position attackPosition;

        public void LoadCard()
        {
            if (currentCard.value == null)
                return;

            currentCard.value.gameObject.SetActive(false);
            cardViz.LoadCard(currentCard.value.viz.card);
            cardViz.gameObject.SetActive(true);
        }

        public void CloseCard()
        {
            cardViz.gameObject.SetActive(false);
            mTransform.localEulerAngles = new Vector3(0, 0, 0);
        }

        private void Start()
        {
            mTransform = this.transform;
            CloseCard();
        }

        void Update()
        {
            mTransform.position = Input.mousePosition;

            if(cardViz.card.cardType == monsterCard)
            {
                List<RaycastResult> results = Settings.GetUIObjs();

                foreach (RaycastResult r in results)
                {
                    Area a = r.gameObject.GetComponentInParent<Area>();

                    if (a != null)
                    {
                        if (a.position == defensePosition)
                        {
                            mTransform.localEulerAngles = new Vector3(0, 0, 90);
                        }
                        else
                        {
                            mTransform.localEulerAngles = new Vector3(0, 0, 0);
                        }
                    }
                }
            }
        }
    }
}