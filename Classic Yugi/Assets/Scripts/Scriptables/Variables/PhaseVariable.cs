﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kzApps
{
    [CreateAssetMenu(menuName = "Variables/Phase")]
    public class PhaseVariable : ScriptableObject
    {
        public Phase value;
    }
}