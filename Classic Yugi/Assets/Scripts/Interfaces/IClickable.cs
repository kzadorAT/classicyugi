﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kzApps
{

    public interface IClickable
    {
        void OnClick();

        void OnHighlight();

    }
}
