﻿using System.Collections;
using System.Collections.Generic;
using kzApps.GameStates;
using UnityEngine;

namespace kzApps
{
    public class GameManager : MonoBehaviour
    {
        [System.NonSerialized]
        public PlayerHolder[] all_players;

        public PlayerHolder GetEnemyOf(PlayerHolder p)
        {
            for (int i = 0; i < all_players.Length; i++)
            {
                if(all_players[i] != p)
                {
                    return all_players[i];
                }
            }

            return null;
        }

        public PlayerHolder currentPlayer;
        public CardHolders playerOneHolder;
        public CardHolders otherPlayersHolder;

        public State currentState;
        public GameObject cardPrefab;

        public int turnIndex;
        public Turn[] turns;
        public SO.GameEvent OnTurnChange;
        public SO.GameEvent OnPhaseChange;
        public SO.StringVariable turnText;

        public PlayerStatsUI[] statsUI;

        public static GameManager singleton;

        private void Awake()
        {
            singleton = this;

            all_players = new PlayerHolder[turns.Length];
            for (int i = 0; i < turns.Length; i++)
            {
                all_players[i] = turns[i].player;
            }

            currentPlayer = turns[0].player;
        }

        private void Start()
        {
            Settings.gameManager = this;

            SetupPlayers();

            turns[0].OnTurnStart();
            turnText.value = turns[turnIndex].player.username;
            OnTurnChange.Raise();
        }

        void SetupPlayers()
        {
            ResourcesManager rm = Settings.GetResourcesManager();

            for (int i = 0; i < all_players.Length; i++)
            {
                all_players[i].Init();
                if(i == 0)
                {
                    all_players[i].currentHolder = playerOneHolder;
                }
                else
                {
                    all_players[i].currentHolder = otherPlayersHolder;
                }
                
                all_players[i].statsUI = statsUI[i];
                all_players[i].currentHolder.LoadPlayer(all_players[i], all_players[i].statsUI);
            }


        }

        public kzApps.Element exisT;

        public void DrawCard(PlayerHolder p)
        {
            if(p.all_cards.Count == 0)
            {
                Debug.Log("Game Over");
                return;
            }
            ResourcesManager rm = Settings.GetResourcesManager();
            string cardId = p.all_cards[0];
            p.all_cards.RemoveAt(0);
            GameObject go = Instantiate(cardPrefab) as GameObject;
            CardViz v = go.GetComponent<CardViz>();
            v.LoadCard(rm.GetCardInstance(cardId));
            CardInstance inst = go.GetComponent<CardInstance>();
            inst.currentLogic = p.handLogic;
            Settings.SetParentForCard(go.transform, p.currentHolder.handGrid.value.transform);
            p.handCards.Add(inst);
        }

        public void LoadPlayerOnActive(PlayerHolder p)
        {
            PlayerHolder prevPlayer = playerOneHolder.playerHolder;
            LoadPlayerOnHolder(prevPlayer, otherPlayersHolder, statsUI[1]);
            LoadPlayerOnHolder(p, playerOneHolder, statsUI[0]);
        }

        public void LoadPlayerOnHolder(PlayerHolder p, CardHolders h, PlayerStatsUI ui)
        {
            h.LoadPlayer(p, ui);
        }


        private void Update()
        {
            bool isComplete = turns[turnIndex].Execute();

            if (isComplete)
            {
                turnIndex++;
                if(turnIndex > turns.Length -1)
                {
                    turnIndex = 0;
                }

                // The current player has changed here
                currentPlayer = turns[turnIndex].player;
                turns[turnIndex].OnTurnStart();
                turnText.value = turns[turnIndex].player.username;
                OnTurnChange.Raise();
            }

            if(currentState != null)
                currentState.Tick(Time.deltaTime);
        }

        public void SetState(State state)
        {
            currentState = state;
        }

        public void EndCurrentPhase()
        {
            Settings.RegisterEvent(turns[turnIndex].name + " finished.", currentPlayer.playerColor);
            turns[turnIndex].EndCurrentPhase();
        }
    } 
}
