﻿using kzApps.GameStates;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kzApps
{

    [CreateAssetMenu(menuName = "Actions/BattlePhaseStartCheck")]
    public class BattlePhaseStartCheck : Condition
    {
        public override bool IsValid()
        {
            GameManager gm = GameManager.singleton;
            PlayerHolder p = gm.currentPlayer;

            int count = gm.currentPlayer.MonsterCards.Count;

            for (int i = 0; i < p.MonsterCards.Count; i++)
            {
                if (p.MonsterCards[i].IsInDefensePosition)
                    count--;
            }

            if(count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
