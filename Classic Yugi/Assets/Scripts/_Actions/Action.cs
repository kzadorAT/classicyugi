﻿using UnityEngine;
using System.Collections;

namespace kzApps.GameStates
{
    public abstract class Action : ScriptableObject
    {
        public abstract void Execute(float d);
    }

}