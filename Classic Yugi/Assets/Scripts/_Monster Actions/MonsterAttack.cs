﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;

namespace kzApps.GameStates
{
    [CreateAssetMenu(menuName = "Actions/MonsterAttack")]
    public class MonsterAttack : Action
    {
        public override void Execute(float d)
        {

            if (Input.GetMouseButtonDown(0))
            {
                List<RaycastResult> results = Settings.GetUIObjs();

                foreach (RaycastResult r in results)
                {
                    CardInstance inst = r.gameObject.GetComponentInParent<CardInstance>();

                    if (!Settings.gameManager.currentPlayer.MonsterCards.Contains(inst))
                        return;

                    if (inst.CanAttack())
                    {
                        Settings.gameManager.currentPlayer.currentAttackingCard = inst;
                    }
                }
            }
        }
    }
}
