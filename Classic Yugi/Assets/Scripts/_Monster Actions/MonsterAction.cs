﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace kzApps
{
    public abstract class MonsterAction : ScriptableObject
    {
        public abstract void Execute(CardInstance cardInst);
    }
}
