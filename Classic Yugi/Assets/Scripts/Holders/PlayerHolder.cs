﻿using System.Collections;
using System.Collections.Generic;
using kzApps.GameElements;
using UnityEngine;

namespace kzApps
{
    [CreateAssetMenu(menuName = "Holders/Player Holder")]
    public class PlayerHolder : ScriptableObject
    {
        public string username;
        public Sprite Portrait;
        public Color playerColor;

        [System.NonSerialized]
        public int health = 8000;
        public PlayerStatsUI statsUI;

        //public string[] startingCards;
        public List<string> startingDeck = new List<string>();
        [System.NonSerialized]
        public List<string> all_cards = new List<string>();

        public bool isHumanPlayer;

        public GE_Logic handLogic;
        public GE_Logic MonsterLogic;
        public GE_Logic SYTLogic;

        [System.NonSerialized]
        public CardHolders currentHolder;

        [System.NonSerialized]
        public List<CardInstance> handCards = new List<CardInstance>();
        [System.NonSerialized]
        public List<CardInstance> SYTCards = new List<CardInstance>();
        [System.NonSerialized]
        public List<CardInstance> MonsterCards = new List<CardInstance>();
        [System.NonSerialized]
        public List<GraveyardHolder> graveyardList = new List<GraveyardHolder>();

        [System.NonSerialized]
        public CardInstance currentAttackingCard;
        
        public void Init()
        {
            health = 8000;
            all_cards.AddRange(startingDeck);
        }

        public int graveyardCount
        {
            get { return currentHolder.graveyardGrid.value.GetComponentsInChildren<CardViz>().Length; }
        }

        public void DropSyTCard(CardInstance inst)
        {
            if (handCards.Contains(inst))
                handCards.Remove(inst);

            SYTCards.Add(inst);

            Settings.RegisterEvent(username + " used " + inst.viz.card.name, playerColor);
        }

        public void DropMonsterCard(CardInstance inst)
        {
            if (handCards.Contains(inst))
                handCards.Remove(inst);

            MonsterCards.Add(inst);

            Settings.RegisterEvent(username + " summon " + inst.viz.card.name + " in "+ (inst.IsInDefensePosition? "defense":"attack") +" position.", playerColor);
        }

        public void AddGraveyardCard(GameObject cardObj)
        {
            GraveyardHolder graveyardHolder = new GraveyardHolder
            {
                cardObj = cardObj
            };

            graveyardList.Add(graveyardHolder);
        }

        public void LoadPlayerOnStatsUI()
        {
            if(statsUI != null)
            {
                statsUI.player = this;
                statsUI.UpdateAll();
            }
        }

        public void DoDamage(int v)
        {
            health -= v;

            if (statsUI != null)
                statsUI.UpdateHealth();
        }

    }
}