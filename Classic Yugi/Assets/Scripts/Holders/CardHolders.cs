﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace kzApps
{
    [CreateAssetMenu(menuName = "Holders/Card Holder")]
    public class CardHolders : ScriptableObject
    {
        public SO.TransformVariable handGrid;
        public SO.TransformVariable graveyardGrid;
        public SO.TransformVariable SYTGrid;
        public SO.TransformVariable monsterGrid;

        [System.NonSerialized]
        public PlayerHolder playerHolder;

        public void LoadPlayer(PlayerHolder p, PlayerStatsUI statsUI)
        {
            if (p == null)
                return;
            playerHolder = p;
            p.currentHolder = this;

            foreach (CardInstance c in p.MonsterCards)
            {
                Settings.SetParentForCard(c.viz.gameObject.transform, monsterGrid.value.transform);
                if (c.IsInDefensePosition)
                    c.transform.localEulerAngles = new Vector3(0, 0, 90);
            }

            foreach (CardInstance c in p.SYTCards)
            {
                Settings.SetParentForCard(c.viz.gameObject.transform, SYTGrid.value.transform);
            }

            foreach (CardInstance c in p.handCards)
            {
                if(c.viz != null)
                {
                    Settings.SetParentForCard(c.viz.gameObject.transform, handGrid.value.transform);
                }
            }

            foreach (GraveyardHolder c in p.graveyardList)
            {
                Settings.SetParentForCard(c.cardObj.transform, graveyardGrid.value.transform);
            }

            p.statsUI = statsUI;
            p.LoadPlayerOnStatsUI();
        }
    }
}
